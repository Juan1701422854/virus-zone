export const environment = {
  production: true,
  apisexo: 'https://virus-zone.herokuapp.com/api/getSexo',
  listacomunas: 'https://virus-zone.herokuapp.com/api/getcomunasV2',
  edad: 'https://virus-zone.herokuapp.com/api/edad',
  comunas: 'https://virus-zone.herokuapp.com/api/getcomunas',
  barrios: 'https://virus-zone.herokuapp.com/api/CasosPorBarrios',
  contagiosMes: 'https://virus-zone.herokuapp.com/api/NumeroPorMes',
  filtros: 'https://virus-zone.herokuapp.com/api/Reportesfiltros',
  listaBarrios: 'https://virus-zone.herokuapp.com/api/Obtenerbarrios',
  puntos: 'https://virus-zone.herokuapp.com/api/comunasGeoreferenciados',
};
