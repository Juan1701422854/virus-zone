// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apisexo: 'http://localhost:1337/api/getSexo',
  listacomunas: 'http://localhost:1337/api/getcomunasV2',
  edad: 'http://localhost:1337/api/edad',
  comunas: 'http://localhost:1337/api/getcomunas',
  barrios: 'http://localhost:1337/api/CasosPorBarrios',
  contagiosMes: 'http://localhost:1337/api/NumeroPorMes',
  filtros: 'http://localhost:1337/api/Reportesfiltros',
  listaBarrios: 'http://localhost:1337/api/Obtenerbarrios',
  puntos: 'http://localhost:1337/api/comunasGeoreferenciados'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
