import { TestBed } from '@angular/core/testing';

import { VariablesReportesService } from './variables-reportes.service';

describe('VariablesReportesService', () => {
  let service: VariablesReportesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VariablesReportesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
