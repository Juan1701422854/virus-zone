import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VariablesReportesService {

  data: any;
  promedio: number;
  desviasionStandar: number;
  mediana: number;
  rangoIntercuatilico: number;
  constructor() {
   }
  private invocarActualizarDatos = new Subject<any>();
  invocarActualizarDatos$ = this.invocarActualizarDatos.asObservable();
   /*
    Funcion que permite llamar el medoto actualizar datos, desde el componente que
    implemente este servicio
    */
  invocarMetodoActualizar(){
    this.invocarActualizarDatos.next();
  }
}
