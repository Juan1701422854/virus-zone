import { Injectable } from '@angular/core';
import { PdfMakeWrapper, Table, Img, Txt } from 'pdfmake-wrapper';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import {ApisService} from './apis.service';
import {VariablesReportesService} from './variables-reportes.service';
import { Chart } from 'chart.js';


@Injectable({
  providedIn: 'root'
})
export class ServicioPdfService {
  // pdf : PdfMakeWrapper;
  dataUrl: string;

  constructor(private api: ApisService, private reporte: VariablesReportesService) { }

  /*
    funcion asincrona para structurar y descargar el pdf
    */
  async descargarPdf(): Promise<void>{
    PdfMakeWrapper.setFonts(pdfFonts);
    const pdf = new PdfMakeWrapper();
    await this.tablasPDf(pdf);
    await this.dibujarGraficaSexo(pdf);
    const labelsAux = [];
    const dataAux = [];
    const coloresAux = [];
    this.generarEdades(labelsAux, dataAux, coloresAux);
    this.DibujarAsincrono(labelsAux, dataAux, coloresAux, pdf);
  }
/*
    funcion que permite dibujar las graficas de sexo, contagios en el mes, 
    para que se realice despues del de edad
    */
  private DibujarAsincrono(labelsAux: any[], dataAux: any[], coloresAux: any[], pdf: PdfMakeWrapper) {
    (async () => {
      await this.delay(2000);
      await this.graficar(labelsAux, dataAux, coloresAux, pdf);
      // tslint:disable-next-line: no-unused-expression
      this.api.obtenerContagiadosPorMes().subscribe(res => {
        const labels = [];
        // tslint:disable-next-line: no-shadowed-variable
        const data = [];
        // tslint:disable-next-line: forin
        const meses = res.meses;
        for (const key in meses) {
          if (meses[key].mes !== 'No Registra') {
            labels.push(meses[key].mes);
            data.push(meses[key].numerodecontagios);
          }
        }
        const titulo = 'Comportamiento del dengue en el año';
        this.GraficarBarras(labels, data, titulo, pdf);
      }).closed;
    })();
  }
  /*
    Funcion que permite obtener las edades para obtener todos los rangos de edades, los cuales tiene como rango
    de diferencia 10 años 
    */
  private generarEdades(labelsAux: any[], dataAux: any[], coloresAux: any[]) {
    for (let index = 0; index < 100; index += 10) {
      const datos = {
        edadinicial: index,
        edadfinal: index + 10
      };
      // tslint:disable-next-line: no-unused-expression
      this.api.obtenerEdad(datos).subscribe(res => {
        labelsAux.push(`${index}-${index + 10}`);
        dataAux.push(res.RangoTotalEdad);
        this.randomColor(coloresAux);
      }).closed;
    }
  }
/*
    Funcion que permite graficar la grafica de contagios por sexo, y agregarlos al pdf
    @pdf el pdf donde se agrega la grafica
    */
  private async dibujarGraficaSexo(pdf: PdfMakeWrapper) {
    await this.api.obtenerSexo().subscribe(async (res) => {
      const labels = [];
      // tslint:disable-next-line: no-shadowed-variable
      const data = [];
      const colores = [];
      // tslint:disable-next-line: forin
      for (const key in res) {
        labels.push(key);
        data.push(res[key]);
        this.randomColor(colores);
        // this.pdf.add(new Img(this.dataUrl));
        // this.pdf.create().download();
      }
      await this.graficar(labels, data, colores, pdf);
    }).closed;
  }
/*
    Funcion que permite crear las tablas en el reporte pdf, dependiendo de los filtros que se
    pongan.
    @pdf el pdf donde se agrega la grafica
    */
  private async tablasPDf(pdf: PdfMakeWrapper) {
    try {
      const data = this.reporte.data;
      data.oportunidad = undefined;
      data.sexo = undefined;
      pdf.info({
        title: 'Informe'
      });
      pdf.add(new Txt('Tabla de contagios').alignment('center').bold().end);
      pdf.add(
        pdf.ln(4)
      );
      await this.api.obtenerInforme(data).subscribe(res => {
        const etiquetas: string[] = [];
        const pacientes: string[] = [];
        // tslint:disable-next-line: forin
        for (const key in res.listapacientes[0]) {
          etiquetas.push(key);
        }
        for (const key of res.listapacientes) {
          pacientes.push(key);
        }
        pdf.add(this.table(pacientes, etiquetas));
        pdf.add(new Txt('Graficas').alignment('center').bold().end);
        pdf.add(
          pdf.ln(4)
        );
      }).closed;
    }
    catch (error) {
    }
  }
/*
    funcion que permite dar una espera, en llamado de otras funciones
    @return una promesa con el tiempo que se desea modificar
    @Param ms es el tiempo que se desea esperar la funcion
    */
  private delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
/*
    Funcion que permite crear el cuerpo de la tabla
    @return el cuerpo de la tabla
    @Param data los datos que vienen con los filtros
    columns el numero de datos, pacientes que corresponden al filtro seleccionado
    */
  private buildTableBody(data: any, columns: any){
    const body = [];
    body.push(columns);
    // tslint:disable-next-line: only-arrow-functions
    data.forEach(function(row) {
        const dataRow = [];
        // tslint:disable-next-line: only-arrow-functions
        columns.forEach(function(column) {
            dataRow.push(row[column].toString());
        });
        body.push(dataRow);
    });
    return body;
  }
  /*
    Funcion que permite crear la tabla y agregar su cuerpo
    @Param data los datos que vienen con los filtros
    columns el numero de datos, pacientes que corresponden al filtro seleccionado
    */
  private table(data, columns) {
    return {
        table: {
            headerRows: 1,
            body: this.buildTableBody(data, columns)
        }
    };
  }
/*
    Funcion que permite generar colores para las graficas
    @Param colores, vector donde se guardan los colores que se van generando
    */
  private randomColor(colores: any): void {
    const simbolos = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color = color + simbolos[Math.floor(Math.random() * 16)];
    }
    if (colores.includes(color)) {
      return this.randomColor(colores);
    }
    else {
      colores.push(color);
    }
  }
/*
    Funcion que permite dibujar los graficos en torta.
    @Param labels, etiquetas de los datos que se visulizan
    data, toda la informacion encesario para graficar, datos numericos.
    colores, lista de colores para pintar la grafica,
    pdf, el pdf donde se agregara la grafica
    */
  private graficar(labels: any[], data: any[], colores: any[], pdf: PdfMakeWrapper): void{
    const canvas = document.getElementById('lienzoFalso') as HTMLCanvasElement;
    let activadorDeEtiquetas = true;
    if (labels.length > 50){
      activadorDeEtiquetas = false;
    }
    const chart = new Chart(canvas.getContext('2d'), {
      type: 'pie',
      data: {
        labels,
        datasets: [{
          data,
          backgroundColor: colores,
          borderColor: colores,
          borderWidth: 1
        }]
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        animation: {
          async onComplete(){
            this.dataUrl = canvas.toDataURL('image/png', 1.0);
            console.log(this.dataUrl);
            pdf.add(await (await new Img(this.dataUrl).alignment('left').height(200).width(500).build()));
            pdf.add(
              pdf.ln(4)
            );
            // pdf.create().download();
          }
        },
        legend: {
          position: 'right',
          display: activadorDeEtiquetas
        },
        scales: {
          yAxes: [{
            gridLines: {
              display: false,
            },
            ticks: {
              beginAtZero: true,
              display: false,
            }
          }]
        }
      }
    });
  }
/*
    Funcion que permite dibujar los graficos en barra.
    @Param labels, etiquetas de los datos que se visulizan
    data, toda la informacion encesario para graficar, datos numericos.
    colores, lista de colores para pintar la grafica,
    pdf, el pdf donde se agregara la grafica
    */
  private GraficarBarras(labels: any[], data: any[], titulo: string, pdf: PdfMakeWrapper) {
    const canvas = document.getElementById('lienzoFalso') as HTMLCanvasElement;
    const chart = new Chart(canvas, {
      type: 'line',
      data: {
        labels,
        datasets: [{
          label: titulo,
          data,
          pointBackgroundColor: 'rgb(155, 158, 180)',
          borderColor: 'rgb(24, 48, 225)',
          fill: false,
          pointRadius: 4
        }]
      },
      options: {
        animation: {
          async onComplete(){
            this.dataUrl = canvas.toDataURL('image/png', 1.0);
            console.log(this.dataUrl);
            pdf.add(await (await new Img(this.dataUrl).alignment('left').height(200).width(500).build()));
            pdf.create().download();
          }
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  }
}
