import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { map } from 'rxjs/operators';
import {environment} from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ApisService {

  constructor(private http: HttpClient) { }

  obtenerContagiadosPorMes(): Observable<any>{
    return this.http.get(environment.contagiosMes)
      .pipe(
          map(res => res)
      );
  }

  obtenerListaComunas(): Observable<any> {
    return this.http.get(environment.comunas)
      .pipe(
        map(res => res)
      );
  }

  obtenerInforme(data: any): Observable<any> {
    const body = JSON.stringify(data);
    return this.http.post(environment.filtros, body)
      .pipe(
        map(res => res)
      );
  }

  obtenerBarrios(data: any): Observable<any> {
    return this.http.post(environment.listaBarrios, data)
      .pipe(
        map(res => res)
      );
  }

  obtenerEdad(data: any): Observable<any> {
    const body = JSON.stringify(data);
    return this.http.post(environment.edad, body)
      .pipe(
        map(res => res)
      );
  }

  obtenerSexo(): Observable<any> {
    return this.http.get(environment.apisexo)
      .pipe(
        map(res => res)
      );
  }

  obtenerPuntosContagio(): Observable<any>{
    return this.http.get(environment.puntos)
      .pipe(
        map(res => res)
      );
  }
}
