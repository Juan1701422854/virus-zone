import { Component, OnInit, HostListener } from '@angular/core';
import { Observable } from 'rxjs/';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Chart } from 'chart.js';
import { async } from '@angular/core/testing';
import * as $ from 'jquery';
import { ServicioComunicacionService } from '../servicio-comunicacion.service';
import {ApisService} from '../Servicios/apis.service';
import {environment} from '../../environments/environment';


@Component({
  selector: 'app-grafica',
  templateUrl: './grafica.component.html',
  styleUrls: ['./grafica.component.css']
})
export class GraficaComponent implements OnInit {
  mensaje: string="";
  chart: Chart;
  private intervalUpdate: any = null;
  constructor(private http: HttpClient, private servicioComunicacion: ServicioComunicacionService,
              private api: ApisService) {
    this.servicioComunicacion.llamarComponenteGrafico$.subscribe(() => {
      this.mostrarBarrios();
    });
  }

  ngOnInit(): void {

  }
/*
    Funcion que permite dibujar la grafica por sexos
    */
  mostrarDatos(): void {
    this.mensaje="contagiados de dengue por sexo";
    this.obtenerSexo().subscribe(res => {
      const labels = [];
      const data = [];
      const colores = [];
      // tslint:disable-next-line: forin
      for (const key in res) {
        labels.push(key);
        data.push(res[key]);
        this.randomColor(colores);
      }
      this.Graficar(labels, data, colores);
    });
  }
/*
    Funcion que permite dibujar la grafica por comunas
    */
  mostrarComunas(): void {
    this.mensaje="contagiados de dengue por comunas";
    this.obtenerComunas().subscribe(res => {
      const labels = [];
      const data = [];
      const colores = [];
      // tslint:disable-next-line: forin
      for (const key in res) {
        labels.push(res[key].comuna);
        data.push(res[key].total);
        this.randomColor(colores);
      }
      this.Graficar(labels, data, colores);
    });
  }
  /*
    Funcion que permite dibujar la grafica por edades
    */
  mostrarEdad(): void {
    this.mensaje="contagiados de dengue por edad";
    const labels = [];
    const data = [];
    const colores = [];
    for (let index = 0; index < 100; index += 10) {
      const datos = {
        edadinicial: index,
        edadfinal: index + 10
      };
      this.obtenerEdad(datos).subscribe(res => {
        labels.push(`${index}-${index + 10}`);
        data.push(res.RangoTotalEdad);
        this.randomColor(colores);
      });
    }
    (async () => {
      await this.delay(2000);
      this.Graficar(labels, data, colores);
    })();
  }

  private delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

/*
    Funcion que permite dibujar la grafica por barrios
    */
  mostrarBarrios(): void {
    this.mensaje="contagiados de dengue por Barrio";
    this.servicioComunicacion.respuesta.subscribe(res => {
      const labels = [];
      const data = [];
      const colores = [];
      const casos = res.casos;
      // tslint:disable-next-line: forin
      for (const key in casos) {
        labels.push(casos[key].nombre);
        data.push(casos[key].cantidad);
        this.randomColor(colores);
      }
      this.Graficar(labels, data, colores);
    });
  }
/*
    Funcion que permite dibujar la grafica por contagios por mes
    */
  mostrarContagiosMes(): void{
    this.mensaje="Patron del dengue";
    this.api.obtenerContagiadosPorMes().subscribe(res => {
      const labels = [];
      const data = [];
      // tslint:disable-next-line: forin
      const meses = res.meses;
      for (const key in meses) {
        if (meses[key].mes !== 'No Registra'){
          labels.push(meses[key].mes);
          data.push(meses[key].numerodecontagios);
        }
      }
      const titulo = 'patron del dengue en el año';
      this.GraficarBarras(labels, data, titulo);
    });
  }
/*
    Funcion que permite dibujar la grafica por torta
    */
  private Graficar(labels: any[], data: any[], colores: any[]) {
    let activadorDeEtiquetas = true;
    if (labels.length > 50){
      activadorDeEtiquetas = false;
    }
    if ($('#canvas')) {
      $('#canvas').remove();
    }
    $('#graph-container').append('<canvas id="canvas"><canvas>');
    this.chart = new Chart('canvas', {
      type: 'pie',
      data: {
        labels,
        datasets: [{
          data,
          backgroundColor: colores,
          borderColor: colores,
          borderWidth: 1
        }]
      },
      options: {
        responsive: true,
        legend: {
          position: 'right',
          display: activadorDeEtiquetas
        },
        scales: {
          yAxes: [{
            gridLines: {
              display: false,
            },
            ticks: {
              beginAtZero: true,
              display: false,
            }
          }]
        }
      }
    });
  }
/*
    Funcion que permite dibujar la grafica por barra/lineas
    */
  private GraficarBarras(labels: any[], data: any[], titulo: string) {
    if ($('#canvas')) {
      $('#canvas').remove();
    }
    $('#graph-container').append('<canvas id="canvas"><canvas>');
    this.chart = new Chart('canvas', {
      type: 'line',
      data: {
        labels,
        datasets: [{
          label: titulo,
          data,
          pointBackgroundColor: 'rgb(155, 158, 180)',
          borderColor: 'rgb(24, 48, 225)',
          fill: false,
          pointRadius: 4
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  }

  private obtenerSexo(): Observable<any> {
    return this.http.get(environment.apisexo)
      .pipe(
        map(res => res)
      );
  }

  private obtenerComunas(): Observable<any> {
    return this.http.get(environment.listacomunas)
      .pipe(
        map(res => res)
      );
  }

  private obtenerEdad(data: any): Observable<any> {
    const body = JSON.stringify(data);
    return this.http.post(environment.edad, body)
      .pipe(
        map(res => res)
      );
  }
  private handleError(err: HttpErrorResponse) { }

  private randomColor(colores: any): void {
    const simbolos = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color = color + simbolos[Math.floor(Math.random() * 16)];
    }
    if (colores.includes(color)) {
      return this.randomColor(colores);
    }
    else {
      colores.push(color);
    }
  }
}
