import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapaGeograficoComponent } from './mapa-geografico.component';

describe('MapaGeograficoComponent', () => {
  let component: MapaGeograficoComponent;
  let fixture: ComponentFixture<MapaGeograficoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapaGeograficoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapaGeograficoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
