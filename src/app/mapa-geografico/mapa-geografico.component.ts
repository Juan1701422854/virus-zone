import { Component, OnInit, ElementRef } from '@angular/core';
import Map from 'ol/Map';
import View from 'ol/View';
import TileLayer from 'ol/layer/Tile';
import Feature from 'ol/Feature';
import * as Proj from 'ol/proj';
import {FullScreen, defaults as defaultControls} from 'ol/control';
import VectorLayer from 'ol/layer/Vector';
import {OSM, Vector as VectorSource} from 'ol/source';
import Point from 'ol/geom/Point';
import {Circle as CircleStyle, Style, Fill, Text, Stroke} from 'ol/style';
import {ApisService} from '../Servicios/apis.service';
import GeoJSON from 'ol/format/GeoJSON';


export const DEFAULT_HEIGHT = '100px';
export const DEFAULT_WIDTH = '100px';

@Component({
  selector: 'app-mapa-geografico',
  templateUrl: './mapa-geografico.component.html',
  styleUrls: ['./mapa-geografico.component.css']
})
export class MapaGeograficoComponent implements OnInit {

  private lat: number;
  private lon: number;
  private zoom: number;
  private width: string | number = DEFAULT_WIDTH;
  private height: string | number = DEFAULT_HEIGHT;
  private mapEl: HTMLElement;
  private map: Map;

  constructor(private elementRef: ElementRef, private api: ApisService) {
    this.lat = 6.2518400;
    this.lon = -75.5635900;
    this.zoom = 12;
    this.width = '500';
    this.height = '500';
   }

  ngOnInit(): void {
    this.mapEl = this.elementRef.nativeElement.querySelector('#map');
    this.setSize();
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngAfterViewInit(): void{
    const estilo = new Style({
      text: new Text({
        font: '12px Calibri,sans-serif',
        fill: new Fill({
          color: '#000',
        }),
      }),
      stroke: new Stroke({
        color: '#319FD3',
        width: 1,
      }),
    });
    const raster = new TileLayer({
      source: new OSM(),
    });
    const vector = new VectorLayer({
      source: new VectorSource({
        url: 'https://opendata.arcgis.com/datasets/283d1d14584641c9971edbd2f695e502_6.geojson',
        format: new GeoJSON(),
      }),
    });
    this.map = new Map({
      target: 'map',
      layers: [raster, vector],
      view: new View({
        center: Proj.fromLonLat([this.lon, this.lat]),
        zoom: this.zoom
      }),
      controls: defaultControls().extend([new FullScreen()]),
    });
    this.dibujarPuntos();
  }

  private setSize(): void{
    if (this.mapEl){
      const styles = this.mapEl.style;
      styles.height = coerceCssPixelValue(this.height) || DEFAULT_HEIGHT;
      styles.width = coerceCssPixelValue(this.width) || DEFAULT_WIDTH;
    }
  }

  private dibujarPuntos(): void{
    const rangos: number[] = this.rangos();
    // tslint:disable-next-line: no-unused-expression
    this.api.obtenerPuntosContagio().subscribe(res => {
      for (const key in res) {
        if (res[key].nombreComuna !== 'Sin informacion'){
          this.pintarPunto(res[key], rangos);
        }
      }
    }).unsubscribe;
  }

  private pintarPunto(comuna: any, rangos: number[]): void{
    let color = '';
    if (comuna.conteo <= rangos[0]){
      color = 'green';
    }
    else if (comuna.conteo > rangos[0] && comuna.conteo <= rangos[1] ){
      color = 'yellow';
    }
    else{
      color = 'red';
    }
    const punto = new Feature({
      geometry: new Point(Proj.fromLonLat([comuna.longitude, comuna.latitude])),
    });
    punto.setStyle(
      new Style({
        image: new CircleStyle({
          radius: 5,
          fill: new Fill({
            color,
        })
      })
    }));
    const vectorSource = new VectorSource({
      features: [punto]
    });
    const vectorLayer = new VectorLayer({
      source: vectorSource,
      map: this.map
    });
  }

  private rangos(): number[]{
    let total = 0;
    let limiterango = 0;
    const rangos = [];
    let totalDatos = 0;
    // tslint:disable-next-line: no-unused-expression
    this.api.obtenerPuntosContagio().subscribe(res => {
      for (const key in res) {
        if (res[key].nombreComuna !== 'Sin informacion'){
          total += res[key].conteo;
          totalDatos += 1;
        }
      }
      limiterango = total / totalDatos;
      rangos.push(limiterango);
      limiterango += limiterango;
      rangos.push(limiterango);
    }).unsubscribe;
    return rangos;
  }
}

const cssUnitsPattern = /([A-Za-z%]+)$/;

function coerceCssPixelValue(value: any): string{
  if (value == null){
    return '';
  }
  return cssUnitsPattern.test(value) ? value : `${value}px`;
}
