import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import {RedNeuronalComponent} from '../red-neuronal/red-neuronal.component';
import {MenuComponent} from '../menu/menu.component';

const routes: Routes = [
  {path: 'red-neuronal', component: RedNeuronalComponent},
  {path: 'pagina-principal', component: MenuComponent},
  {path: '', redirectTo: '/pagina-principal', pathMatch:'full'}
];

@NgModule({
  imports: [
      RouterModule.forRoot(routes)
  ],
  exports: [
      RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }