import { Component, OnInit, Output } from '@angular/core';
import { GraficaComponent } from './../grafica/grafica.component';
import {ServicioPdfService} from '../Servicios/servicio-pdf.service';
import { ServicioComunicacionService } from '../servicio-comunicacion.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  grafico: GraficaComponent;
  constructor(private pdf: ServicioPdfService) { }

  ngOnInit(): void {
  }
   /*
    Funcion que permite descargar el pdf construido
    */
  descargarpdf(): void{
    this.pdf.descargarPdf();
  }
}
