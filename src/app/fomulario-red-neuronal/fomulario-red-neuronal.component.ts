import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fomulario-red-neuronal',
  templateUrl: './fomulario-red-neuronal.component.html',
  styleUrls: ['./fomulario-red-neuronal.component.css']
})
export class FomularioRedNeuronalComponent implements OnInit {

  municipios:string[];
  municipioValue:string;
  alturaValue:number;
  ilv:number;
  ild:number;
  indice_de_beteau:number;
  criaderoPredonimante:number;

  constructor() { }

  ngOnInit(): void {
    this.municipios = ["Manizales",
    "Aguadas",
    "Anserma",
    "Aranzazu",
    "Belarcazar",
    "Chinchina",
    "Filadelfia",
    "La Dorada",
    "La merced",
    "Manzanares",
    "Marmato",
    "Marquetalia",
    "Marulanda",
    "Neira",
    "Norcasia",
    "Pacora",
    "Palestina",
    "Pensilvania",
    "Riosucio",
    "Risaralda",
    "Salamina",
    "Samana",
    "San jose",
    "Supia",
    "Victoria",
    "Villamaria",
    "Viterbo"
  ];
  }

}
