import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FomularioRedNeuronalComponent } from './fomulario-red-neuronal.component';

describe('FomularioRedNeuronalComponent', () => {
  let component: FomularioRedNeuronalComponent;
  let fixture: ComponentFixture<FomularioRedNeuronalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FomularioRedNeuronalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FomularioRedNeuronalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
