import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosRepostesComponent } from './datos-repostes.component';

describe('DatosRepostesComponent', () => {
  let component: DatosRepostesComponent;
  let fixture: ComponentFixture<DatosRepostesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosRepostesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosRepostesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
