import { Component, OnInit } from '@angular/core';
import {VariablesReportesService} from '../Servicios/variables-reportes.service';

@Component({
  selector: 'app-datos-reportes',
  templateUrl: './datos-repostes.component.html',
  styleUrls: ['./datos-repostes.component.css']
})
export class DatosRepostesComponent implements OnInit {

  promedio: number;
  desviasionStandar: number;
  mediana: number;
  rangoIntercuatilico: number;
  constructor(private variablesReporte: VariablesReportesService) {
    this.variablesReporte.invocarActualizarDatos$.subscribe(() => {
      this.actualizarInformacion();
    });
   }

  ngOnInit(): void {}

  /*
    funcion para actualizar las variables necesarios para mostrar datos de desviacion standar,
    promedio, mediana
    */
  private actualizarInformacion(): void{
    this.promedio = this.variablesReporte.promedio;
    this.desviasionStandar = this.variablesReporte.desviasionStandar;
    this.mediana = this.variablesReporte.mediana;
    this.rangoIntercuatilico = this.variablesReporte.rangoIntercuatilico;
  }
}
