import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapaRedNeuronalComponent } from './mapa-red-neuronal.component';

describe('MapaRedNeuronalComponent', () => {
  let component: MapaRedNeuronalComponent;
  let fixture: ComponentFixture<MapaRedNeuronalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapaRedNeuronalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapaRedNeuronalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
