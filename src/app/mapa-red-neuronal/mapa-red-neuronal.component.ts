import { Component, OnInit, ElementRef } from '@angular/core';
import Map from 'ol/Map';
import View from 'ol/View';
import TileLayer from 'ol/layer/Tile';
import Feature from 'ol/Feature';
import * as Proj from 'ol/proj';
import {FullScreen, defaults as defaultControls} from 'ol/control';
import VectorLayer from 'ol/layer/Vector';
import {OSM, Vector as VectorSource} from 'ol/source';
import Point from 'ol/geom/Point';
import {Circle as CircleStyle, Style, Fill, Text, Stroke} from 'ol/style';
import {ApisService} from '../Servicios/apis.service';
import GeoJSON from 'ol/format/GeoJSON';

export const DEFAULT_HEIGHT = '100px';
export const DEFAULT_WIDTH = '100px';

@Component({
  selector: 'app-mapa-red-neuronal',
  templateUrl: './mapa-red-neuronal.component.html',
  styleUrls: ['./mapa-red-neuronal.component.css']
})
export class MapaRedNeuronalComponent implements OnInit {

  private lat: number;
  private lon: number;
  private zoom: number;
  private width: string | number = DEFAULT_WIDTH;
  private height: string | number = DEFAULT_HEIGHT;
  private mapEl: HTMLElement;
  private map: Map;

  constructor(private elementRef: ElementRef, private api: ApisService) {
    this.lat = 5.067;
    this.lon = -75.517;
    this.zoom = 10;
    this.width = '500';
    this.height = '500';
   }

  ngOnInit(): void {
    this.mapEl = this.elementRef.nativeElement.querySelector('#map1');
    this.setSize();
  }
   /*
    Funcion para cargar los elementos del mapa, antes de cargar el ngOnInit,
    y asi ver correctamente el contenedor del mapa
    */
  ngAfterViewInit(): void{
    const raster = new TileLayer({
      source: new OSM(),
    });
    this.map = new Map({
      target: 'map1',
      layers: [raster],
      view: new View({
        center: Proj.fromLonLat([this.lon, this.lat]),
        zoom: this.zoom
      }),
      controls: defaultControls().extend([new FullScreen()]),
    });
  }
  private setSize(): void{
    if (this.mapEl){
      const styles = this.mapEl.style;
      styles.height = coerceCssPixelValue(this.height) || DEFAULT_HEIGHT;
      styles.width = coerceCssPixelValue(this.width) || DEFAULT_WIDTH;
    }
  }
}
const cssUnitsPattern = /([A-Za-z%]+)$/;
 /*
    Funcion que permite modificar las entradas de los tamaños del div que contiene el mapa,
    ingresando solo un numero agrega el px
    @Return el valor modificado con el px
    */
function coerceCssPixelValue(value: any): string{
  if (value == null){
    return '';
  }
  return cssUnitsPattern.test(value) ? value : `${value}px`;
}
