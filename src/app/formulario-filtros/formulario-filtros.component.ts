import { Component, OnInit, Injectable } from '@angular/core';
import {ApisService} from '../Servicios/apis.service';
import { NgbDate, NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import {VariablesReportesService} from '../Servicios/variables-reportes.service';

@Injectable()
export class CustomDateParserFormatter extends NgbDateParserFormatter {

  readonly DELIMITER = '/';

  parse(value: string): NgbDateStruct | null {
    if (value) {
      const date = value.split(this.DELIMITER);
      return {
        day : parseInt(date[0], 10),
        month : parseInt(date[1], 10),
        year : parseInt(date[2], 10)
      };
    }
    return null;
  }

  format(date: NgbDateStruct | null): string {
    return date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : '';
  }
}

@Component({
  selector: 'app-formulario-filtros',
  templateUrl: './formulario-filtros.component.html',
  styleUrls: ['./formulario-filtros.component.css'],
  providers: [
    {provide: NgbDateParserFormatter, useClass: CustomDateParserFormatter}
  ]
})
export class FormularioFiltrosComponent implements OnInit {

  sexos: string[];
  comunas: string[];
  seguridadSocial: string[];
  tipoFiltro: string[];
  sexoValue: string;
  edadMinValue: string;
  edadMaxValue: string;
  comunaValue: string;
  seguridadValue: string;
  oportunidadMinValue: string;
  oportunidadMaxValue: string;
  fechaSintomasMin: string;
  fechaSintomasMax: string;
  tipoCalculo: string;
  fromDate: NgbDate | null;
  toDate: NgbDate | null;

  constructor(private api: ApisService, private variablesReporte: VariablesReportesService) { }

  ngOnInit(): void {
    this.sexos = ['Masculino', 'Femenino'];
    this.comunas = this.obtenercomunas();
    this.seguridadSocial = ['Contributivo', 'Subsidiado', 'Excepcion', 'Especial', 'No asegurado',
    'Indeterminado/Pendiente'];
    this.tipoFiltro = ['Oportunidad', 'Edad'];
  }
 /*
    Funcion que permite consumir el servicio que brinda el las comunas, y las garega
    a una lista
    @Return la lista de comunas
    */
  private obtenercomunas(): any{
    const listadocomunas: string[] = [];
    this.api.obtenerListaComunas().subscribe(res => {
      for (const comuna of res.comunas) {
        listadocomunas.push(comuna);
      }
    });
    return listadocomunas;
  }
/*
    Funcion que permite realizar el consolidado para el infrome de acuerdo a lo que retorna la
     consulta con los filtros
    */
  obtenerConsolidado(): void{
    const data: any = {};
    if (this.sexoValue !== ''){
      if (this.sexoValue === 'Masculino'){
        data.sexo_ = 'M';
      }
      else{
        data.sexo_ = 'F';
      }
    }
    if (this.comunaValue !== undefined) { data.comuna = this.comunaValue; }
    if (this.edadMinValue !== undefined) { data.edad_min = this.edadMinValue; }
    if (this.edadMaxValue !== undefined) { data.edad_max = this.edadMaxValue; }
    if (this.seguridadValue !== undefined){
      if (this.seguridadValue === 'Contributivo') { data.tip_ss_ = 'C'; }
      else if (this.seguridadValue === 'Subsidiado') { data.tip_ss_ = 'S'; }
      else if (this.seguridadValue === 'Excepcion') { data.tip_ss_ = 'P'; }
      else if (this.seguridadValue === 'Especial') { data.tip_ss_ = 'E'; }
      else if (this.seguridadValue === 'No asegurado') { data.tip_ss_ = 'N'; }
      else if (this.seguridadValue === 'Indeterminado/Pendiente') { data.tip_ss_ = 'I'; }
    }
    if (this.oportunidadMinValue !== undefined) { data.oport_min = this.oportunidadMinValue; }
    if (this.oportunidadMaxValue !== undefined) { data.oport_max = this.oportunidadMaxValue; }
    if (this.fechaSintomasMin !== undefined) { data.fechasintomas_min = this.fechaSintomasMin; }
    if (this.fechaSintomasMax !== undefined) { data.fechasintomas_max = this.fechaSintomasMax; }
    if (this.tipoCalculo !== undefined){
      if (this.tipoCalculo === 'Oportunidad') { data.oportunidad = true; }
      else if (this.tipoCalculo === 'Edad') { data.edad = true; }
    }
    this.variablesReporte.data = data;
    this.api.obtenerInforme(data).subscribe(res => {
      this.variablesReporte.promedio = res.promedio;
      this.variablesReporte.mediana = res.mediana;
      this.variablesReporte.desviasionStandar = res.desviacionestandar;
      this.variablesReporte.rangoIntercuatilico = res.rango_intercuartilico;
      this.variablesReporte.invocarMetodoActualizar();
    });
  }
}
