import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioFiltrosComponent } from './formulario-filtros.component';

describe('FormularioFiltrosComponent', () => {
  let component: FormularioFiltrosComponent;
  let fixture: ComponentFixture<FormularioFiltrosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioFiltrosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioFiltrosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
