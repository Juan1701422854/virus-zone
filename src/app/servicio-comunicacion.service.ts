import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServicioComunicacionService {
  respuesta: Observable<any>;
  constructor() { }

  private llamarComponenteGrafico = new Subject<any>();
  llamarComponenteGrafico$ = this.llamarComponenteGrafico.asObservable();
   /*
    Funcion que permite llamar la funcion dibujar grafica del componente grafico, desde el componente que
    implemente este servicio
    */
  llamarMetodoGrafico(){
    this.llamarComponenteGrafico.next();
  }
}
