import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { GraficaComponent } from './grafica/grafica.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { ListaComunasComponent } from './lista-comunas/lista-comunas.component';
import {ApisService} from './Servicios/apis.service';
import { FormularioFiltrosComponent } from './formulario-filtros/formulario-filtros.component'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from '@angular/forms';
import { DatosRepostesComponent } from './datos-repostes/datos-repostes.component';
import {VariablesReportesService} from './Servicios/variables-reportes.service';
import {ServicioPdfService} from './Servicios/servicio-pdf.service';
import { MapaGeograficoComponent } from './mapa-geografico/mapa-geografico.component';
import { RedNeuronalComponent } from './red-neuronal/red-neuronal.component';
import {AppRoutingModule} from './app-routing/app-routing.module';
import { FomularioRedNeuronalComponent } from './fomulario-red-neuronal/fomulario-red-neuronal.component';
import { MapaRedNeuronalComponent } from './mapa-red-neuronal/mapa-red-neuronal.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    GraficaComponent,
    ListaComunasComponent,
    FormularioFiltrosComponent,
    DatosRepostesComponent,
    MapaGeograficoComponent,
    RedNeuronalComponent,
    FomularioRedNeuronalComponent,
    MapaRedNeuronalComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [
    ApisService,
    VariablesReportesService,
    ServicioPdfService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
