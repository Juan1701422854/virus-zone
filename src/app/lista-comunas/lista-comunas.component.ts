import { Component, OnInit, ViewChild, HostBinding, HostListener } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import * as $ from 'jquery';
import { GraficaComponent } from 'src/app/grafica/grafica.component';
import { ServicioComunicacionService } from './../servicio-comunicacion.service';
import {environment} from '../../environments/environment';


@Component({
  selector: 'app-lista-comunas',
  templateUrl: './lista-comunas.component.html',
  styleUrls: ['./lista-comunas.component.css']
})
export class ListaComunasComponent implements OnInit {
  listaComunas = [];
  constructor(private http: HttpClient, private servicioComunicacion: ServicioComunicacionService) {
    this.generarListacomunas();
  }

  ngOnInit(): void {
  }
  /*
    Funcion que permite consumir el servicio que brinda lista de las comunas, mostrarlas en el formulario
    @Return la lista de comunas
    */
  private generarListacomunas(): void {
    this.obtenerListaComunas().subscribe(res => {
      const comunas = [];
      // tslint:disable-next-line: forin
      for (const key in res) {
        comunas.push(res[key]);
      }
      comunas.forEach(comuna => {
        comuna.forEach(nombreComuna => {
          this.listaComunas.push(nombreComuna);
        });
      });
    });
  }
/*
    Funcion que permite enviar la comuna seleccionada para dibujar el grafico correspondiente
    */
  enviar(): void {
    let comunaSeleccionada = '';

    const seleccionarComuna = $('.form-check-input:checked').each(function() {
      comunaSeleccionada = String((($(this).val())));
    });
    const data = {
      comuna: comunaSeleccionada
    };
    const datos: Observable<any> = this.obtenerBarrio(data);
    this.servicioComunicacion.respuesta = datos;
    this.servicioComunicacion.llamarMetodoGrafico();
  }
/*
    Funcion que permite consumir el servicio que brinda el las comunas
    @Return la respuesta del servicio
    */
  private obtenerListaComunas(): Observable<any> {
    return this.http.get(environment.comunas)
      .pipe(
        map(res => res)
      );
  }
  /*
    Funcion que permite consumir el servicio que brinda los barrios
    @Return la respuesta del servicio
    */
  private obtenerBarrio(data: any): Observable<any> {
    const body = JSON.stringify(data);
    return this.http.post(environment.barrios, body)
      .pipe(
        map(res => res)
      );
  }
}
