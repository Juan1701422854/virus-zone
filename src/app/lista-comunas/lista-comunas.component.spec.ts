import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaComunasComponent } from './lista-comunas.component';

describe('ListaComunasComponent', () => {
  let component: ListaComunasComponent;
  let fixture: ComponentFixture<ListaComunasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaComunasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaComunasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
